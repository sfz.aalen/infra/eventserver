{ pkgs }: pkgs.writeShellApplication {
  name = "install";
  text = ''
    set -eo pipefail

    DISK="/dev/sda"
    
    # create new table
    parted --script "$DISK" -- mklabel gpt

    # 0% automatically offsets for optimal performance
    parted --script "$DISK" -- mkpart ESP fat32 0% 512MB
    parted --script "$DISK" -- set 1 esp on

    # main BTRFS partition
    parted --script "$DISK" -- mkpart primary 4GB 100%

    # create BTRFS with all subvolumes
    mkfs.ext4 -L root "$DISK"2
    
    mount "$DISK"2 /mnt
    mkdir -p /mnt/

    # create and mount boot fs
    mkfs.fat -F 32 -n boot "$DISK"1
    mkdir /mnt/boot
    mount "$DISK"1 /mnt/boot

    nixos-install --flake "gitlab:sfz.aalen%2Finfra/eventserver#ccc" --no-root-password
  '';
}
