{
  services.httpd = {
   enable = true;
   logFormat = "combined";
   virtualHosts."eventserver.aalen.space" = 
    {
      documentRoot = "/mnt/data/";
      listen = [{port = 80;}];
      #globalRedirect = "https://k4cg.org";
      #extraConfig = ''
      #Alias /.well-known/acme-challenge/ /var/www/challenges/
      #<Directory /var/www/challenges/>
      #   AllowOverride None
      #   Require all granted
      #   Satisfy Any
      #</Directory>
      #'';
    };
    #{
    #  hostName = "chaostreff-nuernberg.de";
    #  serverAliases = [ "www.chaostreff-nuernberg.de" ];
    #  documentRoot = "/var/www/chaostreff-nuernberg.de/";
    #  listen = [{port = 443;}];
    #  enableSSL = true;
    #  sslServerCert = "/usr/local/acme-tiny/k4cg.org.crt";
    #  sslServerKey = "/usr/local/acme-tiny/k4cg.org.key";
    #  sslServerChain = "/usr/local/acme-tiny/intermediate.crt";
    #}
    #];
  };
}

