{
  networking = {
    hostName = "aalen-server";
    networkmanager.enable = true;
    firewall.enable = true;
    firewall.allowedUDPPorts = [ 12345 ];
    firewall.allowedTCPPorts = [ 21 22 80 443 9999 ];
    #firewall.allowedUDPPorts = [ 51820 ]; # Clients and peers can use the same port, see listenport
    firewall.allowedTCPPortRanges = [ { from = 51000; to = 51999; } ];
    #hosts = {
    #  "10.21.42.193" = [ "vpn.sfz-aalen.space" ];
    #};
  #  wireguard.interfaces = {
  #  # "wg0" is the network interface name. You can name the interface arbitrarily.
  #  wg0 = {
  #    # Determines the IP address and subnet of the client's end of the tunnel interface.
  #    ips = [ "10.19.42.15/24" ];
  #    listenPort = 51820; # to match firewall allowedUDPPorts (without this wg uses random port numbers)

  #    # Path to the private key file.
  #    #
  #    # Note: The private key can also be included inline via the privateKey option,
  #    # but this makes the private key world-readable; thus, using privateKeyFile is
  #    # recommended.
  #    privateKeyFile = "/root/wireguard-keys/private ";

  #    peers = [
  #      # For a client configuration, one peer entry for the server will suffice.

  #      {
  #        # Public key of the server (not a file path).
  #        publicKey = "5FTp3LBBjwxWVEkOHJYvzy5Mab1SzU79X2y5joStVSE=";

  #        # Forward all the traffic via VPN.
  #        allowedIPs = [ "10.19.42.0/24" ];
  #        # Or forward only particular subnets
  #        #allowedIPs = [ "10.100.0.1" "91.108.12.0/22" ];

  #        # Set this to the server IP and port.
  #        endpoint = "10.20.42.2"; # ToDo: route to endpoint not automatically configured https://wiki.archlinux.org/index.php/WireGuard#Loop_routing https://discourse.nixos.org/t/solved-minimal-firewall-setup-for-wireguard-client/7577

  #        # Send keepalives every 25 seconds. Important to keep NAT tables alive.
  #        persistentKeepalive = 25;
  #      }
  #    ];
  #  };
  #};
    #useDHCP = true;
    # Enables DHCP on each ethernet and wireless interface. In case of scripted networking
    # (the default) this is the recommended approach. When using systemd-networkd it's
    # still possible to use this option, but it's recommended to use it in conjunction
    # with explicit per-interface declarations with `networking.interfaces.<interface>.useDHCP`.
    ##networking.useDHCP = lib.mkDefault true;
    # networking.interfaces.enp129s0f0.useDHCP = lib.mkDefault true;
    # networking.interfaces.enp129s0f1.useDHCP = lib.mkDefault true;
    # networking.interfaces.enp129s0f2.useDHCP = lib.mkDefault true;
    # networking.interfaces.enp129s0f3.useDHCP = lib.mkDefault true;
    # networking.interfaces.enp130s0f0.useDHCP = lib.mkDefault true;
    # networking.interfaces.enp130s0f1.useDHCP = lib.mkDefault true;
    # networking.interfaces.enp3s0f0.useDHCP = lib.mkDefault true;
    # networking.interfaces.enp3s0f1.useDHCP = lib.mkDefault true;

    #defaultGateway = "10.20.42.2";
    #nameservers = [ "10.20.42.2" "10.20.42.25" ];

    #interfaces.ens18 = {
    #  useDHCP = false;
    #  ipv4.addresses = [
    #    { address = "10.20.42.150"; prefixLength = 24; }
    #  ];
    #};
  };
}
