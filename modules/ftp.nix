{
    services.vsftpd = {
        enable = true;
	anonymousUser = true;
	anonymousUserNoPassword = true;
        anonymousUserHome = "/mnt/data/";
	anonymousUploadEnable = true;
	anonymousMkdirEnable = true;
        extraConfig = ''
          pasv_enable=Yes
          pasv_min_port=51000
          pasv_max_port=51999
        '';
    };
networking.firewall.allowedTCPPorts = [ 21 ];
networking.firewall.allowedTCPPortRanges = [ { from = 51000; to = 51999; } ];
}
