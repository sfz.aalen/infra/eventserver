{
  environment.shellAliases = {
    system-update = ''
      nixos-rebuild switch --flake gitlab:sfz.aalen%2Finfra/eventserver/$(curl -s https://gitlab.com/api/v4/projects/53276275/repository/commits/ | jq -r -M ".[0].id")#
    '';
  };
}
