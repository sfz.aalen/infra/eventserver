{ pkgs, ... }: {
  environment.systemPackages = with pkgs; [
    vim
    git
    jq
  ];

  imports = [
    ./boot.nix
    ./networking.nix
    ./nix.nix
    ./shell.nix
    ./ssh.nix
    ./user.nix
    ./localisation.nix
    ./datadrive.nix
    ./webserver.nix
    ./ftp.nix
    ./packages.nix
  ];

  system.stateVersion = "23.05";
}

