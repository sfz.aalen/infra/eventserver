{ pkgs, ... }: {
  environment.systemPackages = with pkgs; [
    pkgs.wget
    pkgs.screen
    pkgs.ethtool
    pkgs.tmux
    pkgs.stash
    pkgs.chromium
    pkgs.python3
    pkgs.wireguard-tools
  ];
  services.tailscale.enable = true;
  services.tailscale.port = 12345;
}
